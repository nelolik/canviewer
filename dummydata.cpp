#include "dummydata.h"
#include <QTime>
#include <QColor>
#include "settings.h"
DummyData::DummyData()
{

}

DummyData::DummyData(QVector<DataItem *> *data, QMutex *mutex)
{
    mData = data;
	this->mutex = mutex;
	start();
}

void DummyData::stopDummyThread()
{
	terminate();
}

QVector<DataItem*> *DummyData::createDummyVector(int itemsCount)
{
    QString baseNane = "Item ";
	int canAddr = 0x0CEB0E1;
    int count  = 0;
    QVector<DataItem*> *dummnyVector = new QVector<DataItem*>();
//    QColor colorArray[itemsCount];
	QVector<QColor> colorArray(itemsCount);
    double drand = 0;
	qsrand(static_cast<uint>(QTime::currentTime().msecsSinceStartOfDay()));
    for (int i = 0; i < itemsCount; i++) {
        drand = static_cast<double>(qrand()) / static_cast<double>(RAND_MAX);
        int r = static_cast<int>(200.0 * drand);
        drand = static_cast<double>(qrand()) / static_cast<double>(RAND_MAX);
        int g = static_cast<int>(255.0 * drand);
        drand = static_cast<double>(qrand()) / static_cast<double>(RAND_MAX);
        int b = static_cast<int>(255.0 * drand);
        colorArray[i] = QColor(r, g, b);
    }
    while (count < itemsCount) {
        DataItem *item = new DataItem(baseNane + QString::number(count), true, (new QVector<int>()));
        item->getData()->append(count);
        item->setColor(colorArray[count]);
		item->setCanAddress(canAddr);
		item->setModbusAddress(count + 20);
        dummnyVector->append(item);
        ++count;
    }
	Settings::itemsCount = itemsCount;
    return dummnyVector;
}


void DummyData::run()
{
	int dataCount = mData != nullptr ? mData->size() : 0;
    while (true) {
		mutex->lock();
		dataCount = mData != nullptr ? mData->size() : 0;
		for (int i = 0; i < dataCount; i++) {
			int oldData = 0;
			if (mData->at(i)->getData()->size() > 0) {
				oldData = mData->at(i)->getData()->last();
			}
            if (qrand() > RAND_MAX / 2) {
                oldData += 1;
            } else {
                oldData -= 1;
            }
            mData->at(i)->getData()->append(oldData);
        }
		mutex->unlock();
        msleep(100);

    }
}
