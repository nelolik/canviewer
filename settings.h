#ifndef SETTINGS_H
#define SETTINGS_H

#include "QString"

class Settings
{
public:
	Settings();

	static const QString organization;
	static const QString application;
	static const QString controllerSlavna;
	static const QString controllerSystec;
	static QString controllerName;
	static ulong refreshPeriodMsec;
	static int itemsCount;
	static bool rescalePlot;
	static uint canBaudRate;
};

#endif // SETTINGS_H
