#include "canworkingthread.h"
#include <QVector>
#include <QDebug>

CanWorkingThread::CanWorkingThread()
{

}

CanWorkingThread::CanWorkingThread(CanHandler **canHandler, QVector<DataItem *> *displayedData, QMutex *mutex, QObject *parent) :
	QThread (parent)
{
	pCanHandler = canHandler;
	pDisplayedData = displayedData;
	pMutex = mutex;
	threadRunning = true;
	start();
}

CanWorkingThread::~CanWorkingThread()
{
}

void CanWorkingThread::stopThread()
{
	threadRunning = false;
}


void CanWorkingThread::run()
{
	qDebug() << "Enter working thred run";
	while (threadRunning) {
		if (*pCanHandler == nullptr || !(*pCanHandler)->isReady()) {
			msleep(200);
			continue;
		}
		unsigned long canID;
		QByteArray message = (*pCanHandler)->readAnyMess(&canID);
//		qDebug() << "Message size: " << message.size() << " id: " << canID;
		insertMessage(message, canID);
	}
	qDebug() << "Exit working thred run";
}

void CanWorkingThread::insertMessage(QByteArray &message, unsigned long canID)
{
	bool useData[3] = {false, false, false};

	if (message.size() < 4) { //min 1 dword with address and 1 dword with data
		return;
	}

	char mask = message.at(0);
	for (int i = 0; i < 3; i++) {
		useData[i] = ((mask >> (7 - i)) & 1) == 1;
	}

	int addr = message.at(0);
	addr = ((addr << 8) + message.at(1)) & 0x1FFF;
	for (int i = 0; i < 3; i++) {
		if (useData[i]) {
			int data = message.at(2 + 2 * i);
			data = (data << 8) + message.at(2 + 2 * i + 1);

			foreach (DataItem *di, *pDisplayedData) {
				if (static_cast<unsigned long>(di->getCanAddress()) == canID
						&& di->getModbusAddress() == addr) {
					pMutex->lock();
					di->getData()->append(data);
					pMutex->unlock();
				}
			}

			addr++;
		}
	}
}
