#include "filesaveread.h"
#include <QJsonArray>
#include <QVector>
#include <QTextStream>
#include "dataitem.h"
#include "settings.h"
#include "QFileDialog"

FileSaveRead::FileSaveRead(MainWindow *parent ) :
	QThread ()
{
	mWindow = parent;
	operation = OperationType::NoOp;
}

void FileSaveRead::saveWorkingSet(QString &fileName)
{
	operation = OperationType::SaveWorkingSet;
	file = new QFile(fileName);
	start();
}

void FileSaveRead::loadWorkingSet(QString &fileName)
{
	operation = OperationType::LoadWorkingSet;
	file = new QFile(fileName);
	start();
}

void FileSaveRead::runSaveWorkingSet()
{
	QJsonObject json;
	QJsonArray dataArr;
	foreach (DataItem* item, *(mWindow->displayedData())) {
		QJsonObject itemObject;
		item->write(itemObject);
		dataArr.append(itemObject);
	}
	json["sensors"] = dataArr;
	json["sensors_quantity"] = Settings::itemsCount;
	json["refresh_time"] = static_cast<int>(Settings::refreshPeriodMsec);


	if (!file->open(QIODevice::WriteOnly)) {
		qWarning("Couldn`t open save file");
		return;
	}
	QJsonDocument saveDoc(json);
	file->write(saveDoc.toJson());
	delete file;
	deleteLater();

}

void FileSaveRead::runLoadWorkingSet()
{
	if (!file->open(QIODevice::ReadOnly)) {
		qWarning("Couldn`t open load file");
		return;
	}

	QByteArray loadData = file->readAll();
	QJsonDocument loadDoc = QJsonDocument::fromJson(loadData);
	QJsonObject json = loadDoc.object();
	QJsonArray dataArr = json["sensors"].toArray();
	int defaultCount = dataArr.size();
	QVector<DataItem*> *sensorsFromJson = new QVector<DataItem*>();
	for (int index = 0; index < defaultCount; index++) {
		QJsonObject itemObj = dataArr[index].toObject();
		DataItem *item = new DataItem();
		item->read(itemObj);
		sensorsFromJson->append(item);
	}
	QVector<DataItem*> *mainData = mWindow->displayedData();
	int oldSize = mainData->size();
	mWindow->getMutex().lock();
	mainData->swap(*sensorsFromJson);
	mWindow->applyChanges(oldSize);
	mWindow->getMutex().unlock();

	Settings::itemsCount = json["sensors_quantity"].toInt(defaultCount);
	Settings::refreshPeriodMsec = json["refresh_time"].toInt(Settings::refreshPeriodMsec);

	delete file;
	delete sensorsFromJson;
	deleteLater();
}


void FileSaveRead::run()
{
	if (operation == SaveWorkingSet) {
		runSaveWorkingSet();
	}
	if (operation == LoadWorkingSet) {
		runLoadWorkingSet();
	}
	if (operation == SavePlotData) {
		runSaveScv();
	}
	qDebug() << "Exit file thread run";
}

void FileSaveRead::saveScv(QVector<DataSaveItem*> *data, QString &openFile) {
	operation = OperationType::SavePlotData;
	mData = data;
	file = new QFile(openFile);
	start();
}


void FileSaveRead::runSaveScv() {

	if(file->open(QIODevice::WriteOnly | QIODevice::Text)) {
		QString toFile = "";
		int elementsCount = mData->size();

		for (int i =0; i < elementsCount; i++) {
			QString string = QString::number(static_cast<int>(mData->at(i)->mTime)) + ";";
			int dataSize = mData->at(0)->mData.size();
			for (int j = 0; j < dataSize; j++) {
				string += QString::number(mData->at(i)->mData.at(j)) + ";";
			}
			string += "\n";
			toFile += string;
		}
		QTextStream	stream(file);
		stream << toFile;
		file->close();
	}
	delete file;
	deleteLater();
}
