#-------------------------------------------------
#
# Project created by QtCreator 2018-10-09T09:41:14
#
#-------------------------------------------------

QT       += core gui printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CanViewer
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    dataitem.cpp \
    dummydata.cpp \
    displaytablemodel.cpp \
    qcustomplot.cpp \
    settingsdialog.cpp \
    settingstableadapter.cpp \
    settings.cpp \
    settingsprovider.cpp \
    filesaveread.cpp \
    systec.cpp \
    canworkingthread.cpp \
    canhandler.cpp \
    datasaveitem.cpp \
    slavna.cpp

HEADERS += \
        mainwindow.h \
    dataitem.h \
    dummydata.h \
    displaytablemodel.h \
    qcustomplot.h \
    settingsdialog.h \
    settingstableadapter.h \
    settings.h \
    settingsprovider.h \
    filesaveread.h \
    inc/slcan.h \
    ui_mainwindow.h \
    ui_settingsdialog.h \
    systec.h \
    inc/slcan.h \
    modebox.h \
    canworkingthread.h \
    canhandler.h \
    datasaveitem.h \
    slavna.h

LIBS += -L$$PWD/lib/microsoft -lslcan
LIBS += $$PWD/Systec/Lib/USBCAN32.lib

INCLUDEPATH += $$PWD/inc
INCLUDEPATH += $$PWD/lib/microsoft

FORMS += \
        mainwindow.ui \
    settingsdialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES +=

DISTFILES += \
    slcan.dll \
    USBCAN32.dll
