#ifndef SLAVNA_H
#define SLAVNA_H
#include "canhandler.h"
#include <QObject>
#include "inc/slcan.h"

class Slavna : public QObject, public CanHandler
{
	Q_OBJECT

private:
	HSLCAN hDevice;         //Ссылка на устройство.
	SLCAN_BITRATE br;       //Структура, сохраняющая настройки, отвечающие за тайминги в сети
	SLCAN_MESSAGE OutMsg;   //Структура, сохраняющая сообщение
	PSLCAN_MESSAGE pOutMsg; //Указатель на структуру сообщения
	uint baudRate;          //Переменная хранит индекс сорости сети для взаимодействия графического
	bool isInited;
public:
	Slavna(QObject *parent);
	~Slavna();

signals:
	void signalMessage(QString);

public:
	QByteArray readAnyMess(unsigned long *canID);
	// CanHandler interface
	bool isReady();
	bool init(uint baurRateIdx);
	bool setBaudRate(uint baudRate);
	bool disconnect();
};

#endif // SLAVNA_H
