#ifndef SETTINGSPROVIDER_H
#define SETTINGSPROVIDER_H

#include <QSettings>

class MainWindow;

class SettingsProvider : public QSettings
{
public:
	SettingsProvider(MainWindow *parent = Q_NULLPTR);
	void readSettings();
	void saveSettings();

	static QString TAG_REFRESH_PERIOD;
	static QString TAG_ITEMS_COUNT;
	static QString TAG_CONTROLLER_NAME;

};

#endif // SETTINGSPROVIDER_H
