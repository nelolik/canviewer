#ifndef DATASAVAITEM_H
#define DATASAVAITEM_H

#include <QVector>

class DataSaveItem
{
public:
	DataSaveItem();

	void setTime(double msec);
	void addData(int data);

	double mTime;
	QVector<int> mData;
};

#endif // DATASAVAITEM_H
