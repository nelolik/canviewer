#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "settings.h"

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
}

SettingsDialog::SettingsDialog(QWidget *parent, QVector<DataItem*> *dataContainer) :
    SettingsDialog(parent)
{
	mDataContainer = dataContainer;
	mTmpContainer = new QVector<DataItem*>();
	fillTmpContainer();
	mAdapter = new SettingsTableAdapter(this, mTmpContainer);
    ui->settingsTableView->setModel(mAdapter);
    ui->fieldsCountLineEdit->setText(QString::number(mAdapter->rowsCount()));
	ui->refreshTimeLineEdit->setText(QString::number(Settings::refreshPeriodMsec));
	ui->settingsTableView->setFocus();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
    delete mAdapter;
	delete mTmpContainer;
}

void SettingsDialog::on_buttonBox_accepted()
{
	mDataContainer->swap(*mTmpContainer);
}

void SettingsDialog::fillTmpContainer()
{
	foreach (DataItem* di, *mDataContainer) {
		mTmpContainer->append(new DataItem(di));
	}
}

void SettingsDialog::on_fieldsCountLineEdit_editingFinished()
{
	bool parseOk = false;
	int newSize = ui->fieldsCountLineEdit->text().toInt(&parseOk);
	if (parseOk) {
		mAdapter->changeTableSize(newSize);
		Settings::itemsCount = newSize;
	}
}

void SettingsDialog::on_refreshTimeLineEdit_editingFinished()
{
	bool parseOk = false;
	int newTime = ui->refreshTimeLineEdit->text().toInt(&parseOk);
	if (parseOk) {
		Settings::refreshPeriodMsec = newTime;
	}
}
