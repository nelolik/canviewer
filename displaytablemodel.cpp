#include "displaytablemodel.h"
#include <QEvent>

DisplayTableModel::DisplayTableModel() : QAbstractTableModel ()
{

}

DisplayTableModel::DisplayTableModel(QObject *parent, QVector<DataItem *> *displayedData) :
    QAbstractTableModel (parent)
{
    mDisplayedData = displayedData;
    allowRefresh = true;
}

DisplayTableModel::~DisplayTableModel()
{

}

void DisplayTableModel::valueColumnChanged()
{
    int rowCount = mDisplayedData->size();
    for (int i = 0; i < rowCount; i++) {
        emit dataChanged(index(i,1),
                         index(i, 1));
	}
}

void DisplayTableModel::updateTable(int oldSize)
{
	if (oldSize > 0) {
		beginRemoveRows(QModelIndex(), 0, oldSize - 1);
		endRemoveRows();
	}
	if (mDisplayedData->size() > 0) {
		beginInsertRows(QModelIndex(), 0, mDisplayedData->size() - 1);
		endInsertRows();
	}
}

int DisplayTableModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return mDisplayedData->size();
}

int DisplayTableModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return 4;
}

QVariant DisplayTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
	int row = index.row();
	DataItem *item = mDisplayedData->at(row);
	if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        int column = index.column();
        switch (column) {
		case 0:
			return item->getName();
        case 1:
			return item->getData()->size() > 0 ? item->getData()->last() : 0;

        }
    }

    if (role == Qt::CheckStateRole && index.column() == 2)
    {
        return item->getShowGraph();
    }
	if (role == Qt::DecorationRole && index.column() == 3) {
        return item->getColor();
	}

    return QVariant();
}

QVariant DisplayTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole || orientation != Qt::Horizontal)
    {
        return QVariant();
    }

    if (section == 0) {
        return tr("Название");
    } else if (section == 1) {
        return tr("Значение");
	}
	return QVariant();
}


Qt::ItemFlags DisplayTableModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractTableModel::flags(index);
    if(!index.isValid() )
        return flags;
    if (index.column() == 0) {
        return flags | Qt::ItemIsEditable;
    }
    if (index.column() == 2) {
        return Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsEditable;
    }
    return flags;
}


bool DisplayTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid()) {
        return false;
    }

    if (role == Qt::CheckStateRole) {
        if (index.column() == 2) {
            int row = index.row();
            bool checkstate = mDisplayedData->at(row)->getShowGraph();
            mDisplayedData->at(row)->setShowGraph(!checkstate);
            emit dataChanged(index,index);
            return true;
        }
    }
    if (role == Qt::EditRole) {
        if (index.column() == 0) {
            allowRefresh = true;
            QString newName = value.toString();
            int row = index.row();
            mDisplayedData->at(row)->setName(newName);
            emit dataChanged(index,index);
            return true;
        }
    }
    return false;
}




bool DisplayTableModel::insertRows(int row, int count, const QModelIndex &parent)
{
	Q_UNUSED(parent)
	if (row <= mDisplayedData->size()) {
		while (count-- > 0) {
			mDisplayedData->insert(row, new DataItem());
		}
		return true;
	}
	return false;
}

bool DisplayTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
	Q_UNUSED(parent)
	if (row < mDisplayedData->size() && count <= (mDisplayedData->size() - row)) {
		mDisplayedData->remove(row, count);
		return true;
	}

	return false;
}
