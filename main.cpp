#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	QStringList paths = QCoreApplication::libraryPaths();
	paths.append(".");
	paths.append("imageformats");
	paths.append("platforms");
	paths.append("sqldrivers");
	QCoreApplication::setLibraryPaths(paths);
	QCoreApplication::addLibraryPath("./");

	QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
