#include "settingstableadapter.h"

SettingsTableAdapter::SettingsTableAdapter()
{

}

SettingsTableAdapter::SettingsTableAdapter(QObject *parent, QVector<DataItem*> *addresses) :
    QAbstractTableModel (parent)
{
    mAddresses = addresses;

}


int SettingsTableAdapter::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return mAddresses->size();
}

int SettingsTableAdapter::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return 2;
}

QVariant SettingsTableAdapter::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
	if(role == Qt::DisplayRole || role == Qt::EditRole) {
        if(index.column() == 0) {
			return QString::number(mAddresses->at(index.row())->getCanAddress(), 16);
        } else if (index.column() == 1) {
			return mAddresses->at(index.row())->getModbusAddress();
		}
    }
    return QVariant();
}

int SettingsTableAdapter::rowsCount()
{
	return mAddresses->size();
}

void SettingsTableAdapter::updateTable()
{
	int lastRow = mAddresses->size() - 1;
	emit dataChanged(index(0, 0),
					 index(lastRow, 1));

}

void SettingsTableAdapter::changeTableSize(int newSize)
{
	int oldSize = rowsCount();
	if (newSize > oldSize) {
		beginInsertRows(QModelIndex(), oldSize, newSize - 1);
		insertRows(oldSize, newSize - oldSize, QModelIndex());
		endInsertRows();
	}
	if (newSize < oldSize) {
		beginRemoveRows(QModelIndex(), newSize, oldSize - 1);
		removeRows(newSize, oldSize - newSize, QModelIndex());
		endRemoveRows();
	}
}


bool SettingsTableAdapter::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (!index.isValid()) {
		return false;
	}
	if (role == Qt::EditRole) {
		int row = index.row();
		int column = index.column();
		if(column == 0) {
			bool ok;
			int newAddr = value.toString().toInt(&ok, 16);
			if (ok) {
				mAddresses->at(row)->setCanAddress(newAddr);
			}
		}
		if (column == 1) {
			mAddresses->at(row)->setModbusAddress(value.toInt());
		}
		emit dataChanged(index, index);
		return true;
	}

	return false;
}

QVariant SettingsTableAdapter::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role != Qt::DisplayRole)
	{
		return QVariant();
	}

	if (orientation == Qt::Horizontal) {
		if (section == 0) {
			return tr("CAN адрес");
		} else {
			return tr("Modbus адрес");
		}
	}
	return section;
}


Qt::ItemFlags SettingsTableAdapter::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (!index.isValid()) {
		return flags;
	}
	return flags | Qt::ItemIsEditable | Qt::ItemIsSelectable;

}


bool SettingsTableAdapter::insertRows(int row, int count, const QModelIndex &parent)
{
	Q_UNUSED(parent)
	if (count > 0 && row >= mAddresses->size()) {
		while(count-- > 0) {
			mAddresses->insert(row, new DataItem());
		}
		return true;
	}
	return false;
}

bool SettingsTableAdapter::removeRows(int row, int count, const QModelIndex &parent)
{
	Q_UNUSED(parent)
	if (row < mAddresses->size() && count <=(mAddresses->size() - row)) {
		while (count-- > 0) {
			mAddresses->removeAt(row);
		}
		return true;
	}
	return false;
}
