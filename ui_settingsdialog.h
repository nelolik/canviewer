/********************************************************************************
** Form generated from reading UI file 'settingsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGSDIALOG_H
#define UI_SETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SettingsDialog
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *refreshTimeHLayout;
    QLineEdit *refreshTimeLineEdit;
    QLabel *refreshTimeLbl;
    QHBoxLayout *fieldsCountHLayout;
    QLineEdit *fieldsCountLineEdit;
    QLabel *fieldsCountLabel;
    QTableView *settingsTableView;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SettingsDialog)
    {
        if (SettingsDialog->objectName().isEmpty())
            SettingsDialog->setObjectName(QStringLiteral("SettingsDialog"));
        SettingsDialog->resize(480, 640);
        verticalLayout = new QVBoxLayout(SettingsDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        refreshTimeHLayout = new QHBoxLayout();
        refreshTimeHLayout->setObjectName(QStringLiteral("refreshTimeHLayout"));
        refreshTimeLineEdit = new QLineEdit(SettingsDialog);
        refreshTimeLineEdit->setObjectName(QStringLiteral("refreshTimeLineEdit"));
        refreshTimeLineEdit->setMinimumSize(QSize(30, 0));
        refreshTimeLineEdit->setMaximumSize(QSize(30, 16777215));
        refreshTimeLineEdit->setInputMethodHints(Qt::ImhDigitsOnly);

        refreshTimeHLayout->addWidget(refreshTimeLineEdit);

        refreshTimeLbl = new QLabel(SettingsDialog);
        refreshTimeLbl->setObjectName(QStringLiteral("refreshTimeLbl"));

        refreshTimeHLayout->addWidget(refreshTimeLbl);


        verticalLayout->addLayout(refreshTimeHLayout);

        fieldsCountHLayout = new QHBoxLayout();
        fieldsCountHLayout->setObjectName(QStringLiteral("fieldsCountHLayout"));
        fieldsCountLineEdit = new QLineEdit(SettingsDialog);
        fieldsCountLineEdit->setObjectName(QStringLiteral("fieldsCountLineEdit"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(fieldsCountLineEdit->sizePolicy().hasHeightForWidth());
        fieldsCountLineEdit->setSizePolicy(sizePolicy);
        fieldsCountLineEdit->setMinimumSize(QSize(30, 0));
        fieldsCountLineEdit->setMaximumSize(QSize(30, 16777215));
        fieldsCountLineEdit->setInputMethodHints(Qt::ImhDigitsOnly);

        fieldsCountHLayout->addWidget(fieldsCountLineEdit);

        fieldsCountLabel = new QLabel(SettingsDialog);
        fieldsCountLabel->setObjectName(QStringLiteral("fieldsCountLabel"));

        fieldsCountHLayout->addWidget(fieldsCountLabel);


        verticalLayout->addLayout(fieldsCountHLayout);

        settingsTableView = new QTableView(SettingsDialog);
        settingsTableView->setObjectName(QStringLiteral("settingsTableView"));
        settingsTableView->setAutoScroll(true);

        verticalLayout->addWidget(settingsTableView);

        buttonBox = new QDialogButtonBox(SettingsDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(SettingsDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), SettingsDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SettingsDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(SettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *SettingsDialog)
    {
        SettingsDialog->setWindowTitle(QApplication::translate("SettingsDialog", "Dialog", Q_NULLPTR));
        refreshTimeLbl->setText(QApplication::translate("SettingsDialog", "\320\237\320\265\321\200\320\270\320\276\320\264 \320\276\320\261\320\275\320\276\320\262\320\273\320\265\320\275\320\270\321\217, \320\274\321\201\320\265\320\272", Q_NULLPTR));
        fieldsCountLabel->setText(QApplication::translate("SettingsDialog", "\320\232\320\276\320\273\320\270\321\207\320\265\321\201\321\202\320\262\320\276 \320\277\320\276\320\273\320\265\320\271 \320\264\320\260\320\275\320\275\321\213\321\205", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class SettingsDialog: public Ui_SettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGSDIALOG_H
