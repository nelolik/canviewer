#ifndef CANHANDLER_H
#define CANHANDLER_H

#include "QByteArray"

class CanHandler
{
public:
	CanHandler();
	virtual ~CanHandler()=0;

	virtual QByteArray readAnyMess(unsigned long *canID)=0;
	virtual bool isReady()=0;
	virtual bool init(uint baurRate)=0;
	virtual bool disconnect()=0;
	virtual bool setBaudRate(uint baudRate)=0;
};

#endif // CANHANDLER_H
