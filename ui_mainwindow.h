/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "../../qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionSettings;
    QAction *actionSave;
    QAction *actionLoad;
    QAction *actionStop;
    QAction *actionPlot;
    QAction *actionFit_Horisontal;
    QAction *actionSave_data;
    QAction *action125_kBit_s;
    QAction *action250_kBit_s;
    QAction *action500_kBit_s;
    QAction *action1_MBit_s;
    QAction *actionReconnect;
    QAction *actionDisconnect;
    QAction *actionSLAVNA;
    QAction *actionSYSTEC;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QTableView *tableView;
    QCustomPlot *plotWidget;
    QMenuBar *menuBar;
    QMenu *menuMenu;
    QMenu *menuPlot;
    QMenu *menuDevise;
    QMenu *menuSpeed;
    QMenu *menuUSB_interface;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(975, 612);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        actionSettings = new QAction(MainWindow);
        actionSettings->setObjectName(QStringLiteral("actionSettings"));
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionLoad = new QAction(MainWindow);
        actionLoad->setObjectName(QStringLiteral("actionLoad"));
        actionStop = new QAction(MainWindow);
        actionStop->setObjectName(QStringLiteral("actionStop"));
        actionPlot = new QAction(MainWindow);
        actionPlot->setObjectName(QStringLiteral("actionPlot"));
        actionPlot->setCheckable(true);
        actionPlot->setChecked(true);
        actionFit_Horisontal = new QAction(MainWindow);
        actionFit_Horisontal->setObjectName(QStringLiteral("actionFit_Horisontal"));
        actionFit_Horisontal->setCheckable(true);
        actionFit_Horisontal->setChecked(true);
        actionSave_data = new QAction(MainWindow);
        actionSave_data->setObjectName(QStringLiteral("actionSave_data"));
        action125_kBit_s = new QAction(MainWindow);
        action125_kBit_s->setObjectName(QStringLiteral("action125_kBit_s"));
        action125_kBit_s->setCheckable(true);
        action250_kBit_s = new QAction(MainWindow);
        action250_kBit_s->setObjectName(QStringLiteral("action250_kBit_s"));
        action250_kBit_s->setCheckable(true);
        action250_kBit_s->setChecked(true);
        action500_kBit_s = new QAction(MainWindow);
        action500_kBit_s->setObjectName(QStringLiteral("action500_kBit_s"));
        action500_kBit_s->setCheckable(true);
        action1_MBit_s = new QAction(MainWindow);
        action1_MBit_s->setObjectName(QStringLiteral("action1_MBit_s"));
        action1_MBit_s->setCheckable(true);
        actionReconnect = new QAction(MainWindow);
        actionReconnect->setObjectName(QStringLiteral("actionReconnect"));
        actionDisconnect = new QAction(MainWindow);
        actionDisconnect->setObjectName(QStringLiteral("actionDisconnect"));
        actionSLAVNA = new QAction(MainWindow);
        actionSLAVNA->setObjectName(QStringLiteral("actionSLAVNA"));
        actionSLAVNA->setCheckable(true);
        actionSLAVNA->setChecked(false);
        actionSYSTEC = new QAction(MainWindow);
        actionSYSTEC->setObjectName(QStringLiteral("actionSYSTEC"));
        actionSYSTEC->setCheckable(true);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        tableView = new QTableView(centralWidget);
        tableView->setObjectName(QStringLiteral("tableView"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(1);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tableView->sizePolicy().hasHeightForWidth());
        tableView->setSizePolicy(sizePolicy1);

        horizontalLayout->addWidget(tableView);

        plotWidget = new QCustomPlot(centralWidget);
        plotWidget->setObjectName(QStringLiteral("plotWidget"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(2);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(plotWidget->sizePolicy().hasHeightForWidth());
        plotWidget->setSizePolicy(sizePolicy2);
        plotWidget->setMinimumSize(QSize(500, 0));

        horizontalLayout->addWidget(plotWidget);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 975, 21));
        menuMenu = new QMenu(menuBar);
        menuMenu->setObjectName(QStringLiteral("menuMenu"));
        menuPlot = new QMenu(menuBar);
        menuPlot->setObjectName(QStringLiteral("menuPlot"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(menuPlot->sizePolicy().hasHeightForWidth());
        menuPlot->setSizePolicy(sizePolicy3);
        menuPlot->setLayoutDirection(Qt::LeftToRight);
        menuDevise = new QMenu(menuBar);
        menuDevise->setObjectName(QStringLiteral("menuDevise"));
        menuSpeed = new QMenu(menuDevise);
        menuSpeed->setObjectName(QStringLiteral("menuSpeed"));
        menuUSB_interface = new QMenu(menuDevise);
        menuUSB_interface->setObjectName(QStringLiteral("menuUSB_interface"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuMenu->menuAction());
        menuBar->addAction(menuPlot->menuAction());
        menuBar->addAction(menuDevise->menuAction());
        menuMenu->addAction(actionSettings);
        menuMenu->addAction(actionSave);
        menuMenu->addAction(actionLoad);
        menuPlot->addAction(actionPlot);
        menuPlot->addAction(actionFit_Horisontal);
        menuPlot->addAction(actionSave_data);
        menuDevise->addAction(menuSpeed->menuAction());
        menuDevise->addAction(menuUSB_interface->menuAction());
        menuDevise->addAction(actionReconnect);
        menuDevise->addAction(actionDisconnect);
        menuSpeed->addAction(action125_kBit_s);
        menuSpeed->addAction(action250_kBit_s);
        menuSpeed->addAction(action500_kBit_s);
        menuSpeed->addAction(action1_MBit_s);
        menuUSB_interface->addAction(actionSLAVNA);
        menuUSB_interface->addAction(actionSYSTEC);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "CanViewer", Q_NULLPTR));
        actionSettings->setText(QApplication::translate("MainWindow", "Settings", Q_NULLPTR));
        actionSave->setText(QApplication::translate("MainWindow", "Save", Q_NULLPTR));
        actionLoad->setText(QApplication::translate("MainWindow", "Load", Q_NULLPTR));
        actionStop->setText(QApplication::translate("MainWindow", "Stop", Q_NULLPTR));
        actionPlot->setText(QApplication::translate("MainWindow", "Fit Vertical", Q_NULLPTR));
        actionFit_Horisontal->setText(QApplication::translate("MainWindow", "Fit Horizontal", Q_NULLPTR));
        actionSave_data->setText(QApplication::translate("MainWindow", "Save data", Q_NULLPTR));
        action125_kBit_s->setText(QApplication::translate("MainWindow", "125 kBit/s", Q_NULLPTR));
        action250_kBit_s->setText(QApplication::translate("MainWindow", "250 kBit/s", Q_NULLPTR));
        action500_kBit_s->setText(QApplication::translate("MainWindow", "500 kBit/s", Q_NULLPTR));
        action1_MBit_s->setText(QApplication::translate("MainWindow", "1 MBit/s", Q_NULLPTR));
        actionReconnect->setText(QApplication::translate("MainWindow", "Reconnect", Q_NULLPTR));
        actionDisconnect->setText(QApplication::translate("MainWindow", "Disconnect", Q_NULLPTR));
        actionSLAVNA->setText(QApplication::translate("MainWindow", "SLAVNA", Q_NULLPTR));
        actionSYSTEC->setText(QApplication::translate("MainWindow", "SYSTEC", Q_NULLPTR));
        menuMenu->setTitle(QApplication::translate("MainWindow", "Menu", Q_NULLPTR));
        menuPlot->setTitle(QApplication::translate("MainWindow", "Plot", Q_NULLPTR));
        menuDevise->setTitle(QApplication::translate("MainWindow", "Device", Q_NULLPTR));
        menuSpeed->setTitle(QApplication::translate("MainWindow", "Speed", Q_NULLPTR));
        menuUSB_interface->setTitle(QApplication::translate("MainWindow", "USB interface", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
