#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QMultiMap>
#include "settingstableadapter.h"
#include "dataitem.h"

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = nullptr);
	SettingsDialog(QWidget *parent, QVector<DataItem*> *dataContainer);
    ~SettingsDialog();

private slots:
	void on_buttonBox_accepted();

	void on_fieldsCountLineEdit_editingFinished();

	void on_refreshTimeLineEdit_editingFinished();

private:
    Ui::SettingsDialog *ui;
	QVector<DataItem*> *mDataContainer;
	QVector<DataItem*> *mTmpContainer;
	QList<int> mKeys, mVals;
    SettingsTableAdapter *mAdapter;

	void makeFlatAddressContainer();
	void fillTmpContainer();
};

#endif // SETTINGSDIALOG_H
