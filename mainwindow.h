#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "dataitem.h"
#include "datasaveitem.h"
#include "displaytablemodel.h"
#include "qcustomplot.h"
#include "settingsprovider.h"
#include "systec.h"
#include "canworkingthread.h"
#include "slavna.h"

namespace Ui {
class MainWindow;
}

class DummyData;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

	QVector<DataItem *> *displayedData() const;
	void applyChanges(int oldSize);

	QMutex &getMutex();

public slots:
	void updateDataSlot();

private slots:
    void on_actionSettings_triggered();

	void on_tableView_clicked(const QModelIndex &index);


	void on_actionSave_triggered();

	void on_actionLoad_triggered();

	void onMessageFromUsbInterfase(QString);

	void on_actionPlot_triggered(bool checked);

	void on_actionFit_Horisontal_triggered(bool checked);

	void on_actionSave_data_triggered();

	void on_action125_kBit_s_triggered();

	void on_action250_kBit_s_triggered();

	void on_action500_kBit_s_triggered();

	void on_action1_MBit_s_triggered();

	void on_actionReconnect_triggered();

	void on_actionDisconnect_triggered();

	void on_actionSLAVNA_triggered();

	void on_actionSYSTEC_triggered();

private:
    Ui::MainWindow *ui;
    QVector<DataItem*> *mDisplayedData;
	QVector<DataSaveItem*> *mReadyToSaveData;
	DisplayTableModel *mDisplayTableModel;
	ulong refreshPeriodMsec;
    QTimer timer;
	SettingsProvider settingsProvider;
	CanWorkingThread *mWorkingThread;
	DummyData *dummnyThred;
	QMutex mutex;
	CanHandler *canHandler;
	Slavna *canSlavna;
	Systec *canSystec;
	bool rescalePlotX;
	bool rescalePlotY;

	enum  Controller{
		slavna,
		systec
	};

	Controller controller;

	void setTableWidth();
	void createCanThred();
	void setupCustomPlot(QCustomPlot *customPLot);
	void resetCustomPlot(QCustomPlot *customPlot);
	void setControllerFromSettings();


	// QWidget interface
protected:
	void closeEvent(QCloseEvent *event);
};

#endif // MAINWINDOW_H
