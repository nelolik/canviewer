#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dummydata.h"
#include <QTime>
#include <QString>
#include <QDebug>
#include "settingsdialog.h"
#include "settings.h"
#include "settingsprovider.h"
#include "filesaveread.h"
#include "slavna.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
	ui(new Ui::MainWindow),
	settingsProvider(this)
{
    ui->setupUi(this);
	setWindowTitle(tr("CanViewer"));
	settingsProvider.readSettings();
	refreshPeriodMsec = Settings::refreshPeriodMsec;
	rescalePlotX = Settings::rescalePlot;
	rescalePlotY = Settings::rescalePlot;
//    mDisplayedData = DummyData::createDummyVector(32);
	mDisplayedData = new QVector<DataItem*>();
	mReadyToSaveData = new QVector<DataSaveItem*>();
//	dummnyThred = new DummyData(mDisplayedData, &mutex);

	timer.start(refreshPeriodMsec);
    connect(&timer, SIGNAL(timeout()), this, SLOT(updateDataSlot()));

	mDisplayTableModel = new DisplayTableModel(this, mDisplayedData);
	ui->tableView->setModel(mDisplayTableModel);
	ui->tableView->resizeColumnsToContents();
	setTableWidth();
	setupCustomPlot(ui->plotWidget);
	setControllerFromSettings();
//	controller = systec;
	canSlavna = nullptr;
	canSystec = nullptr;
	canHandler = nullptr;
	switch (controller) {
	case slavna:
		canHandler = new Slavna(this);
//		connect(canSlavna, SIGNAL(signalMessage(QString)), this, SLOT(onMessageFromUsbInterfase(QString)), Qt::QueuedConnection);
		break;
	case systec:
		canHandler = new Systec(this);
//		connect(canSystec, SIGNAL(signalMessage(QString)), this, SLOT(onMessageFromUsbInterfase(QString)), Qt::QueuedConnection);
		break;
	}
	canHandler->init(Settings::canBaudRate);
	mWorkingThread = new CanWorkingThread(&canHandler, mDisplayedData, &mutex, this);

	QString fileName = "lastState.json";
	FileSaveRead *saver = new FileSaveRead(this);
	saver->loadWorkingSet(fileName);
}

MainWindow::~MainWindow()
{
	settingsProvider.saveSettings();
	QString fileName = "lastState.json";
	FileSaveRead *saver = new FileSaveRead(this);
	saver->saveWorkingSet(fileName);
	timer.stop();
//	dummnyThred->stopDummyThread();
	//	delete dummnyThred;
	if (mWorkingThread != nullptr) {
		mWorkingThread->stopThread();
		mWorkingThread->wait(refreshPeriodMsec + 10);
	}
	delete ui;
	if (mDisplayedData != nullptr) {
		foreach(DataItem *i, *mDisplayedData) {
			delete i;
		}
		delete mDisplayedData;
	}
	if (mReadyToSaveData != nullptr) {
		foreach(DataSaveItem *i, *mReadyToSaveData) {
			delete i;
		}
		delete mReadyToSaveData;
	}
	delete mDisplayTableModel;
	qDebug() << "Model deleted";
	delete canSystec;
	delete canSlavna;
	qDebug() << "Can deleted";

	delete mWorkingThread;
	qDebug() << "Workind thread stoped";
	saver->wait(1000);
	qDebug() << "destructor end";
}

void MainWindow::setTableWidth()
{
	int columnCount = mDisplayTableModel->columnCount(QModelIndex());
    int width = 0;
    for (int i = 0; i < columnCount; i++) {
        width += ui->tableView->columnWidth(i);
    }
    width += 25;    //ширина скролбара
	ui->tableView->setFixedWidth(width);
}

void MainWindow::setupCustomPlot(QCustomPlot *customPlot)
{
    int plotCount = mDisplayedData->size();
	customPlot->clearGraphs();
    for (int j = 0; j < plotCount; j++) {
        customPlot->addGraph();
        customPlot->graph(j)->setPen(QPen(mDisplayedData->at(j)->getColor()));
        if (mDisplayedData->at(j)->getData()->size() > 0) {
            customPlot->graph(j)->addData(0, mDisplayedData->at(j)->getData()->last());
        } else {
            customPlot->graph(j)->addData(0, 0);
        }
    }
    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

}

void MainWindow::resetCustomPlot(QCustomPlot *customPlot)
{
	int newPlotCount = mDisplayedData->size();
	int oldPlotCount = customPlot->graphCount();
	if (newPlotCount < oldPlotCount) {
		for (int i = oldPlotCount - 1; i >= newPlotCount; customPlot->removeGraph(i--));
	}
	if (newPlotCount > oldPlotCount) {
		for (int i = oldPlotCount; i < newPlotCount; i++) {
			customPlot->addGraph();
			customPlot->graph(i)->setPen(QPen(mDisplayedData->at(i)->getColor()));
			if (mDisplayedData->at(i)->getData()->size() > 0) {
				customPlot->graph(i)->addData(0, mDisplayedData->at(i)->getData()->last());
			} else {
				customPlot->graph(i)->addData(0, 0);
			}
		}
	}
}

void MainWindow::setControllerFromSettings()
{
	if (Settings::controllerName == Settings::controllerSlavna) {
		controller = slavna;
		ui->actionSLAVNA->setChecked(true);
		ui->actionSYSTEC->setChecked(false);
	}
	if (Settings::controllerName == Settings::controllerSystec) {
		controller = systec;
		ui->actionSLAVNA->setChecked(false);
		ui->actionSYSTEC->setChecked(true);
	}
}

void MainWindow::applyChanges(int oldSize)
{
	resetCustomPlot(ui->plotWidget);
	mDisplayTableModel->updateTable(oldSize);
	if (refreshPeriodMsec != Settings::refreshPeriodMsec) {
		refreshPeriodMsec = Settings::refreshPeriodMsec;
		timer.stop();
		timer.start(refreshPeriodMsec);
	}
}

void MainWindow::updateDataSlot()
{
    static QTime time(QTime::currentTime());
    double elapsed = time.elapsed() / 1000.0;

    int plotCount = mDisplayedData->size();
	if(plotCount != ui->plotWidget->graphCount()) {
		applyChanges(ui->plotWidget->graphCount());
		return;
	}
	DataSaveItem *pSaveItem = new DataSaveItem();
	pSaveItem->setTime(elapsed*1000);
	mutex.lock();
	for (int j = 0; j < plotCount; j++) {
		if (mDisplayedData->at(j)->getData()->size() > 0) {
            int last = mDisplayedData->at(j)->getData()->last();
            ui->plotWidget->graph(j)->addData(elapsed, last);
			pSaveItem->addData(last);
            mDisplayedData->at(j)->getData()->clear();
            mDisplayedData->at(j)->getData()->append(last);
            ui->plotWidget->graph(j)->setVisible(mDisplayedData->at(j)->getShowGraph());
        }
    }
	mutex.unlock();
	mReadyToSaveData->append(pSaveItem);
	if (rescalePlotX) {
		ui->plotWidget->yAxis->rescale(false);
	}
	if (rescalePlotY) {
		ui->plotWidget->xAxis->setRange(elapsed, 100, Qt::AlignRight);
	}
    ui->plotWidget->replot();
	mDisplayTableModel->valueColumnChanged();
}

void MainWindow::on_actionSettings_triggered()
{
	int oldSize = mDisplayedData->size();
	SettingsDialog settingsDialog(this, mDisplayedData);
	settingsDialog.setWindowTitle("Settings");
	int result = settingsDialog.exec();
	if (result == SettingsDialog::Accepted) {
		applyChanges(oldSize);
	}
}

void MainWindow::on_actionSave_triggered()
{
	FileSaveRead *saver = new FileSaveRead(this);
	QFileDialog dialog(this, tr("Сохранить как"), "", "*.json");
	QString fileName = dialog.getSaveFileName(this, tr("Сохранить как"), "",
											  "Json file (*.json);;All files (*)");
	saver->saveWorkingSet(fileName);
}

void MainWindow::on_actionLoad_triggered()
{
	FileSaveRead *saver = new FileSaveRead(this);
	QFileDialog dialog(this);
	QString fileName = dialog.getOpenFileName(this, tr("Загрузить файл"), "",
											  "Json file (*.json);;All files (*)");

	saver->loadWorkingSet(fileName);

}

QMutex &MainWindow::getMutex()
{
	return mutex;
}

void MainWindow::on_tableView_clicked(const QModelIndex &index)
{
	if (!index.isValid()) {
		return;
	}

	if (index.column() == 3) {
		QColorDialog dialog(mDisplayedData->at(index.row())->getColor(), this);
//		dialog.exec();
		QColor newColor = dialog.getColor(mDisplayedData->at(index.row())->getColor(), this);
		if (newColor.isValid()) {
			mDisplayedData->at(index.row())->setColor(newColor);
			ui->plotWidget->graph(index.row())->setPen(QPen(newColor));
			ui->plotWidget->replot();
		}
	}
}

QVector<DataItem *> *MainWindow::displayedData() const
{
	return mDisplayedData;
}


void MainWindow::closeEvent(QCloseEvent *event)
{
	Q_UNUSED(event)
//	canSystec->disconnest();
	settingsProvider.saveSettings();
}


void MainWindow::onMessageFromUsbInterfase(QString msg)
{
	statusBar()->showMessage(msg);
}



void MainWindow::on_actionPlot_triggered(bool checked)
{
	rescalePlotX = checked;
}

void MainWindow::on_actionFit_Horisontal_triggered(bool checked)
{
	rescalePlotY = checked;
}

void MainWindow::on_actionSave_data_triggered()
{
	FileSaveRead *fileOp = new FileSaveRead(this);
	QFileDialog dialog(this, tr("Сохранить как"), "", "*.csv");
	QString fileName = dialog.getSaveFileName(this, tr("Сохранить как"), "",
											  "Csv file (*.csv);;All files (*)");
	fileOp->saveScv(mReadyToSaveData, fileName);
}

void MainWindow::on_action125_kBit_s_triggered()
{
	Settings::canBaudRate = 0;
	canHandler->setBaudRate(Settings::canBaudRate);
	ui->action125_kBit_s->setChecked(true);
	ui->action250_kBit_s->setChecked(false);
	ui->action500_kBit_s->setChecked(false);
	ui->action1_MBit_s->setChecked(false);
}

void MainWindow::on_action250_kBit_s_triggered()
{
	Settings::canBaudRate = 1;
	canHandler->setBaudRate(Settings::canBaudRate);
	ui->action125_kBit_s->setChecked(false);
	ui->action250_kBit_s->setChecked(true);
	ui->action500_kBit_s->setChecked(false);
	ui->action1_MBit_s->setChecked(false);
}

void MainWindow::on_action500_kBit_s_triggered()
{
	Settings::canBaudRate = 2;
	canHandler->setBaudRate(Settings::canBaudRate);
	ui->action125_kBit_s->setChecked(false);
	ui->action250_kBit_s->setChecked(false);
	ui->action500_kBit_s->setChecked(true);
	ui->action1_MBit_s->setChecked(false);
}

void MainWindow::on_action1_MBit_s_triggered()
{
	Settings::canBaudRate = 3;
	canHandler->setBaudRate(Settings::canBaudRate);
	ui->action125_kBit_s->setChecked(false);
	ui->action250_kBit_s->setChecked(false);
	ui->action500_kBit_s->setChecked(false);
	ui->action1_MBit_s->setChecked(true);
}

void MainWindow::on_actionReconnect_triggered()
{
	if (canHandler != nullptr) {
		canHandler->init(Settings::canBaudRate);
	}
}

void MainWindow::on_actionDisconnect_triggered()
{
	if (canHandler != nullptr) {
		canHandler->disconnect();
	}
}

void MainWindow::on_actionSLAVNA_triggered()
{
	if (controller == slavna) {
		if (canHandler == nullptr) {
			canHandler = new Slavna(this);
		}
		if (!canHandler->isReady()) {
			canHandler->init(Settings::canBaudRate);
		}
	};
	if (controller == systec) {
		controller = slavna;
		canHandler->disconnect();
		//Чтобы в цикле чтения can не обращались к удалённому объекту
		CanHandler *tmp = canHandler;
		canHandler = nullptr;
		delete tmp;
		canHandler = new Slavna(this);
		qDebug() << canHandler->init(Settings::canBaudRate);
	}

	ui->actionSLAVNA->setChecked(true);
	ui->actionSYSTEC->setChecked(false);
}

void MainWindow::on_actionSYSTEC_triggered()
{
	if (controller == systec) {
		if (canHandler == nullptr) {
			canHandler = new Systec(this);
		}
		if (!canHandler->isReady()) {
			canHandler->init(Settings::canBaudRate);
		}
	}
	qDebug() << "Между ифами";
	if (controller == slavna) {
		controller = systec;
		canHandler->disconnect();
		qDebug() << "Disconnected";
		//Чтобы в цикле чтения can не обращались к удалённому объекту
		CanHandler *tmp = canHandler;
		canHandler = nullptr;
		delete tmp;
		qDebug() << "Deleted";
		tmp = new Systec(this);
		qDebug() << tmp->init(Settings::canBaudRate);
		canHandler = tmp;
	}
	ui->actionSLAVNA->setChecked(false);
	ui->actionSYSTEC->setChecked(true);
}
