#ifndef FILESAVEREAD_H
#define FILESAVEREAD_H

#include <QThread>
#include "mainwindow.h"
#include "datasaveitem.h"



class FileSaveRead : public QThread
{
private:
	enum OperationType {
		SaveWorkingSet = 0,
		LoadWorkingSet = 1,
		SavePlotData = 2,
		NoOp = 3
	};

public:
	FileSaveRead(MainWindow *parent);
	void saveWorkingSet(QString &fileName);
	void loadWorkingSet(QString &fileName);
	void saveScv(QVector<DataSaveItem *> *data, QString &openFile);


private:
	MainWindow *mWindow;
	OperationType operation;
	QFile *file;
	QVector<DataSaveItem *> *mData;

	void runSaveWorkingSet();
	void runLoadWorkingSet();
	void runSaveScv();


	// QThread interface
protected:
	void run();
};

#endif // FILESAVEREAD_H
