#include "dataitem.h"
#include <QVector>
#include <QJsonObject>
#include <QJsonArray>

DataItem::DataItem()
{
    name = "";
    showGraph = false;
	data = new QVector<int>();
	canAddress = 0;
	modbusAddress = 0;
	color = Qt::red;
}

DataItem::DataItem(DataItem *dataItem)
{
	name = dataItem->name;
	showGraph = dataItem->showGraph;
	data = new QVector<int>(*(dataItem->data));
	canAddress = dataItem->canAddress;
	modbusAddress = dataItem->modbusAddress;
	color = dataItem->color;
}

DataItem::DataItem(QString name, bool showGraph, QVector<int> *data)
{
    this->name = name;
    this->showGraph = showGraph;
    this->data = data;
	canAddress = 0;
	modbusAddress = 0;
	color = Qt::blue;
}

DataItem::~DataItem()
{
    delete data;
}

bool DataItem::getShowGraph() const
{
    return showGraph;
}

void DataItem::setShowGraph(bool value)
{
    showGraph = value;
}

QVector<int> *DataItem::getData() const
{
    return data;
}

void DataItem::setData(QVector<int> *value)
{
    data = value;
}

QColor DataItem::getColor() const
{
    return color;
}

void DataItem::setColor(const QColor &value)
{
    color = value;
}

int DataItem::getCanAddress() const
{
    return canAddress;
}

void DataItem::setCanAddress(int value)
{
    canAddress = value;
}

int DataItem::getModbusAddress() const
{
    return modbusAddress;
}

void DataItem::setModbusAddress(int value)
{
	modbusAddress = value;
}

void DataItem::write(QJsonObject &json)
{
	json["name"] = name;
	json["show"] = showGraph;
	json["can_addr"] = canAddress;
	json["modbus_addr"] = modbusAddress;
	int r, g, b;
	color.getRgb(&r, &g, &b);
	QJsonArray colors;
	colors.append(r);
	colors.append(g);
	colors.append(b);
	json["color"] = colors;
}

void DataItem::read(QJsonObject &json)
{
	name = json["name"].toString("Val");
	showGraph = json["show"].toBool(true);
	QJsonArray colors = json["color"].toArray();
	int r = colors[0].toInt(0);
	int g = colors[1].toInt(0);
	int b = colors[2].toInt(0);
	color = QColor(r, g, b);
	canAddress = json["can_addr"].toInt(0);
	modbusAddress = json["modbus_addr"].toInt(0);
}

QString DataItem::getName() const
{
    return name;
}

void DataItem::setName(const QString &value)
{
    name = value;
}

