#ifndef DATAITEM_H
#define DATAITEM_H

#include <QString>
#include <QColor>

class QJsonObject;

class DataItem
{
public:
    DataItem();
	DataItem(DataItem *dataItem);
    DataItem(QString name, bool showGraph, QVector<int> *data);
    ~DataItem();
    QString getName() const;
    void setName(const QString &value);
    bool getShowGraph() const;
    void setShowGraph(bool value);

    QVector<int> *getData() const;
    void setData(QVector<int> *value);

    QColor getColor() const;
    void setColor(const QColor &value);

	int getCanAddress() const;
	void setCanAddress(int value);

	int getModbusAddress() const;
	void setModbusAddress(int value);

	void read(QJsonObject &json);
	void write(QJsonObject &json);

private:
	QString name;
	bool showGraph;
	QVector<int> *data;
    QColor color;
	int canAddress;
	int modbusAddress;

};

#endif // DATAITEM_H
