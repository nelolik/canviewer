#include "settingsprovider.h"
#include "settings.h"
#include "mainwindow.h"

SettingsProvider::SettingsProvider(MainWindow *parent) :
	QSettings (Settings::organization, Settings::application, parent)
{

}

QString SettingsProvider::TAG_REFRESH_PERIOD = "common/refresh_period";
QString SettingsProvider::TAG_ITEMS_COUNT = "common/items_count";
QString SettingsProvider::TAG_CONTROLLER_NAME = "common/controller_name";

void SettingsProvider::readSettings()
{
	Settings::refreshPeriodMsec = value(TAG_REFRESH_PERIOD, static_cast<int>(Settings::refreshPeriodMsec)).toUInt();
	Settings::itemsCount = value(TAG_ITEMS_COUNT, Settings::itemsCount).toInt();
	Settings::controllerName = value(TAG_CONTROLLER_NAME, Settings::controllerName).toString();
}


void SettingsProvider::saveSettings()
{
	setValue(TAG_REFRESH_PERIOD, static_cast<int>(Settings::refreshPeriodMsec));
	setValue(TAG_ITEMS_COUNT, Settings::itemsCount);
	setValue(TAG_CONTROLLER_NAME, Settings::controllerName);
}
