#ifndef DISPLAYTABLEMODEL_H
#define DISPLAYTABLEMODEL_H

#include <QAbstractTableModel>
#include <QVector>
#include "dataitem.h"

class DisplayTableModel : public QAbstractTableModel
{
public:
    DisplayTableModel();
    DisplayTableModel(QObject *parent, QVector<DataItem*> *displayedData);
    ~DisplayTableModel();
	void valueColumnChanged();
	void updateTable(int oldSize);


    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

private:
    QVector<DataItem*> *mDisplayedData;
    bool allowRefresh;

    // QAbstractItemModel interface
public:
    Qt::ItemFlags flags(const QModelIndex &index) const;

    // QAbstractItemModel interface
public:
    bool setData(const QModelIndex &index, const QVariant &value, int role);



	// QAbstractItemModel interface
public:
	bool insertRows(int row, int count, const QModelIndex &parent);
	bool removeRows(int row, int count, const QModelIndex &parent);
};

#endif // DISPLAYTABLEMODEL_H
