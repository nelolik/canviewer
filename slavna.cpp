#include "slavna.h"

Slavna::Slavna(QObject *parent) : QObject (parent)
{
	isInited = false;
}

Slavna::~Slavna() {
	disconnect();
}

bool Slavna::disconnect() {
	isInited = false;
	return SlCan_Free(false);
}


QByteArray Slavna::readAnyMess(unsigned long *canID)
{
	SLCAN_MESSAGE InMess[10];
	BOOL b;
	int timeOut = 100;
	ULONG readCount = 0;
	if(hDevice == INVALID_HANDLE_VALUE) {
		return QByteArray();
	}
	b = SlCan_DeviceReadMessages(hDevice, timeOut,InMess, 1, &readCount);
	if(!b) {
		return QByteArray();
	}
	QByteArray msg;
	msg.append(static_cast<char>(InMess[0].Data[4]))
		.append(static_cast<char>(InMess[0].Data[5]))
		.append(static_cast<char>(InMess[0].Data[6]))
		.append(static_cast<char>(InMess[0].Data[7]))
		.append(static_cast<char>(InMess[0].Data[0]))
		.append(static_cast<char>(InMess[0].Data[1]))
		.append(static_cast<char>(InMess[0].Data[2]))
			.append(static_cast<char>(InMess[0].Data[3]));
	*canID = InMess[0].ID;
	return msg;

}

bool Slavna::isReady()
{
	return isInited;
}


bool Slavna::init(uint baurRateIdx)
{
	BOOL b;
	ULONG devicecount;
	baudRate = baurRateIdx;
	// Загрузим библиотеку. Необходимо это сделать один раз
	// до вызова любой другой функции библиотеки.
	b=SlCan_Load(NULL,NULL);
	if(b == FALSE) {
		emit signalMessage("Библиотека SlCan не загружена");
		return false;
	}


	// Выясним, сколько устройств доступно
	devicecount = SlCan_GetDeviceCount();
	if(devicecount <= 0) {
		emit signalMessage("Устройства не доступны");
		return false;
	}
	// Получим ссылку на первое устройство
	hDevice = SlCan_GetDevice(0);

	if (hDevice == INVALID_HANDLE_VALUE) {
		emit signalMessage("Ссылка на устройство не получена");
		return false;
	}
	// Откроем устройство.
	b = SlCan_DeviceOpen(hDevice);
	if (b == FALSE) {
		emit signalMessage("Устройство не открылось");
		return false;
	}

	b = setBaudRate(baudRate);
	if(b)
	{
		// Установим режим приёма и передачи
		b = SlCan_DeviceSetMode(hDevice,SLCAN_MODE_NORMAL);
		isInited = true;
		emit signalMessage("Slavna подключен");
		return true;
	} else
	{
		emit signalMessage("Ошибка установки скорости");
		return false;
	}
}

bool Slavna::setBaudRate(uint baudRate)
{
	if(hDevice == INVALID_HANDLE_VALUE) {
		return false;
	}
	switch (baudRate) {
	case 0:
		br.BRP = SLCAN_BR_CIA_125K;
		break;
	case 1:
		br.BRP = SLCAN_BR_CIA_250K;
		break;
	case 2:
		br.BRP = SLCAN_BR_CIA_500K;
		break;
	case 3:
		br.BRP = SLCAN_BR_CIA_1000K;
		break;
	default:
		break;
	}
	return SlCan_DeviceSetBitRate(hDevice,&br);
}
