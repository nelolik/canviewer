#ifndef CANWORKINGTHREAD_H
#define CANWORKINGTHREAD_H

#include <QThread>
#include <QMutex>
#include "canhandler.h"
#include "dataitem.h"



class CanWorkingThread : public QThread
{
public:
	CanWorkingThread();
	CanWorkingThread(CanHandler **canHandler, QVector<DataItem*> *displayedData, QMutex *mutex, QObject *parent);
	~CanWorkingThread();
	void stopThread();

	// QThread interface
protected:
	void run();

private:
	CanHandler **pCanHandler;
	QVector<DataItem*> *pDisplayedData;
	QMutex *pMutex;
	bool threadRunning;

	void insertMessage(QByteArray &message, unsigned long canID);
};

#endif // CANWORKINGTHREAD_H
