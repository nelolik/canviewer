#ifndef DUMMYDATA_H
#define DUMMYDATA_H

#include <QThread>
#include <QVector>
#include <dataitem.h>
#include <QMutex>

class DummyData : QThread
{
public:
    DummyData();
	DummyData(QVector<DataItem *> *data, QMutex *mutex);
    QVector<DataItem *> *mData;
	QMutex *mutex;

	void stopDummyThread();


    static QVector<DataItem *> *createDummyVector(int itemsCount);

    // QThread interface
protected:
    void run();
};

#endif // DUMMYDATA_H
