#include "settings.h"

Settings::Settings()
{

}


const QString Settings::organization = "NII_SET";
const QString Settings::application = "Can Viewer";
const QString Settings::controllerSlavna = "slavna";
const QString Settings::controllerSystec = "systec";
QString Settings::controllerName = "slavna";
ulong Settings::refreshPeriodMsec = 1000;
int Settings::itemsCount = 1;
bool Settings::rescalePlot = true;
uint Settings::canBaudRate = 1;	//0 - 125; 1 - 250; 2 - 500; 3 - 1000 kBit/s
