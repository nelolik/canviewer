#include "systec.h"
#include "modebox.h"

#include <QDebug>
#include <QTime>

Systec::Systec(QObject *parent) : QObject (parent)
{
	deviceNbr = 0;
	isInited = false;
//	reInit(baud);
}

Systec::~Systec()
{
	disconnect();
}

bool Systec::disconnect()
{
	isInited = false;
	int ret = UcanDeinitCan(UcanHandle);
	if(ret != USBCAN_SUCCESSFUL)
	{
		error(ret);
	} else {
		qDebug() << "UcanDeinitCan";
	}
	ret = UcanDeinitHardware(UcanHandle);
	if(ret != USBCAN_SUCCESSFUL)
	{
		error(ret);
	} else {
		qDebug() << "UcanDeinitHardware";
	}

	emit signalMessage("Systec отключен");
	return true;
}

bool Systec::getIsInited() const
{
	return isInited;
}

void Systec::error(int err)
{
	switch(err)
	{
	case USBCAN_SUCCESSFUL:
        qDebug() << "USBCAN_SUCCESSFUL";
		emit signalMessage("USBCAN_SUCCESSFUL");
        break;
    case USBCAN_ERR_MAXINSTANCES:
        qDebug() << "USBCAN_ERR_MAXINSTANCES";
		emit signalMessage("USBCAN_ERR_MAXINSTANCES");
        break;
    case USBCAN_ERR_ILLHANDLE:
        qDebug() << "USBCAN_ERR_ILLHANDLE";
		emit signalMessage("USBCAN_ERR_ILLHANDLE");
        break;
    case USBCAN_ERR_RESOURCE:
        qDebug() << "USBCAN_ERR_RESOURCE";
		emit signalMessage("USBCAN_ERR_RESOURCE");
        break;
    case USBCAN_ERR_BUSY:
        qDebug() << "USBCAN_ERR_BUSY";
		emit signalMessage("USBCAN_ERR_BUSY");
        break;
    case USBCAN_ERR_IOFAILED:
        qDebug() << "USBCAN_ERR_IOFAILED";
		emit signalMessage("USBCAN_ERR_IOFAILED");
        break;
    case USBCAN_ERR_ILLPARAM:
        qDebug() << "USBCAN_ERR_ILLPARAM";
		emit signalMessage("USBCAN_ERR_ILLPARAM");
        break;
    case USBCAN_ERR_ILLHW:
        qDebug() << "USBCAN_ERR_ILLHW";
		emit signalMessage("USBCAN_ERR_ILLHW");
        break;
    case USBCAN_ERR_DISCONNECT:
        qDebug() << "USBCAN_ERR_DISCONNECT";
		emit signalMessage("USBCAN_ERR_DISCONNECT");
        break;
    case USBCAN_ERR_TIMEOUT:
        qDebug() << "USBCAN_ERR_TIMEOUT";
		emit signalMessage("USBCAN_ERR_TIMEOUT");
        break;
    case USBCAN_ERRCMD:
        qDebug() << "USBCAN_ERRCMD";
		emit signalMessage("USBCAN_ERRCMD");
        break;
    case USBCAN_ERR_CANNOTINIT:
        qDebug() << "USBCAN_ERR_CANNOTINIT";
		emit signalMessage("USBCAN_ERR_CANNOTINIT");
        break;
    case USBCAN_ERR_ILLCHANNEL:
        qDebug() << "USBCAN_ERR_ILLCHANNEL";
		emit signalMessage("USBCAN_ERR_ILLCHANNEL");
        break;
    case USBCAN_WARN_NODATA:
        qDebug() << "USBCAN_WARN_NODATA";
		emit signalMessage("USBCAN_WARN_NODATA");
        break;
    case USBCAN_WARN_SYS_RXOVERRUN:
        qDebug() << "USBCAN_WARN_SYS_RXOVERRUN";
		emit signalMessage("USBCAN_WARN_SYS_RXOVERRUN");
        break;
    case USBCAN_WARN_DLL_RXOVERRUN:
        qDebug() << "USBCAN_WARN_DLL_RXOVERRUN";
		emit signalMessage("USBCAN_WARN_DLL_RXOVERRUN");
        break;
    case USBCAN_WARN_FW_RXOVERRUN:
        qDebug() << "USBCAN_WARN_FW_RXOVERRUN";
		emit signalMessage("USBCAN_WARN_FW_RXOVERRUN");
        break;
    case USBCAN_ERR_DLL_TXFULL:
        qDebug() << "USBCAN_ERR_DLL_TXFULL";
		emit signalMessage("USBCAN_ERR_DLL_TXFULL");
        break;
    case USBCAN_WARN_FW_TXOVERRUN:
        qDebug() << "USBCAN_WARN_FW_TXOVERRUN";
		emit signalMessage("USBCAN_WARN_FW_TXOVERRUN");
        break;
	}
}

QByteArray Systec::readAnyMess(unsigned long *canID)
{
    tCanMsgStruct InMsg;
    UCANRET ret;
	QByteArray data;
	ret = UcanReadCanMsg(UcanHandle, &InMsg);
	if(USBCAN_CHECK_VALID_RXCANMSG(ret))
	{
		data.append(static_cast<char>(InMsg.m_bData[4])).append(static_cast<char>(InMsg.m_bData[5]))
				.append(static_cast<char>(InMsg.m_bData[6])).append(static_cast<char>(InMsg.m_bData[7]))
				.append(static_cast<char>(InMsg.m_bData[0])).append(static_cast<char>(InMsg.m_bData[1]))
				.append(static_cast<char>(InMsg.m_bData[2])).append(static_cast<char>(InMsg.m_bData[3]));
		*canID = InMsg.m_dwID;
	}
	else
	{
		if(ret != USBCAN_WARN_NODATA) { error(ret);}
	}
	return data;
}

void Systec::writeTo()
{
	tCanMsgStruct OutMsg;
    UCANRET ret;

	//Посылантся команда 2 для подготовки контроллера к приёму данных
    OutMsg.m_dwID = SEND_BOX_ID;
    OutMsg.m_bFF = USBCAN_MSG_FF_EXT;
    OutMsg.m_bDLC = 8;
    OutMsg.m_bData[4] = 0xE0;
    OutMsg.m_bData[5] = SEND_DATA_ADRESS;
	OutMsg.m_bData[6] = 0;
	OutMsg.m_bData[7] = 0;
	OutMsg.m_bData[0] = 0;
	OutMsg.m_bData[1] = 0;
    OutMsg.m_bData[2] = 0;
	OutMsg.m_bData[3] = 0;
    ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
	if(!USBCAN_CHECK_TX_SUCCESS(ret)) {
		error(ret);
    }

}

bool Systec::isReady()
{
	return isInited;
}




bool Systec::init(uint baurRate)
{
	int ret;
	switch (baurRate) {
	case 0:
		baudRate = USBCAN_BAUD_125kBit;
		break;
	case 1:
		baudRate =  USBCAN_BAUD_250kBit;
		break;
	case 2:
		baudRate = USBCAN_BAUD_500kBit;
		break;
	case 3:
		baudRate = USBCAN_BAUD_1MBit;
		break;
	default:
		baudRate = USBCAN_BAUD_250kBit;
		break;
	}
	if (isInited) {
		ret = UcanDeinitCan(UcanHandle);
		if(ret != USBCAN_SUCCESSFUL) {
			error(ret);
			qDebug() << "Deinit can error";
		}
		ret = UcanDeinitHardware(UcanHandle);
		if(ret != USBCAN_SUCCESSFUL) {
			error(ret);
			qDebug() << "Deinit hardware error";
		}
	}


	ret = UcanInitHardware(&UcanHandle, deviceNbr, nullptr);


	if(ret != USBCAN_SUCCESSFUL) {
		error(ret);
		return false;
	}
	ret = UcanInitCan(UcanHandle, HIBYTE(baudRate), LOBYTE(baudRate), 0xFFFFFFFF, 0);
	if(ret != USBCAN_SUCCESSFUL)
	{
		error(ret);
		return false;
	}
	isInited = true;
	emit signalMessage("Systec подключен");
	return true;

}

bool Systec::setBaudRate(uint baudRateIdx)
{
	switch (baudRateIdx) {
	case 0:
		baudRate = USBCAN_BAUD_125kBit;
		break;
	case 1:
		baudRate =  USBCAN_BAUD_250kBit;
		break;
	case 2:
		baudRate = USBCAN_BAUD_500kBit;
		break;
	case 3:
		baudRate = USBCAN_BAUD_1MBit;
		break;
	default:
		baudRate = USBCAN_BAUD_250kBit;
		break;
	}
	int ret = UcanSetBaudrate(UcanHandle, HIBYTE(baudRate), LOBYTE(baudRate));
	if(ret != USBCAN_SUCCESSFUL) {
		error(ret);
		return false;
	}
	return true;
}
