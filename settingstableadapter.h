#ifndef SETTINGSTABLEADAPTER_H
#define SETTINGSTABLEADAPTER_H

#include <QAbstractTableModel>
#include <QMultiMap>
#include "dataitem.h"



class SettingsTableAdapter : public QAbstractTableModel
{
public:
    SettingsTableAdapter();
	SettingsTableAdapter(QObject *parent, QVector<DataItem*> *addresses);

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;

    int rowsCount();
	void updateTable();
	void changeTableSize(int newSize);

private:
	QVector<DataItem*> *mAddresses;

	// QAbstractItemModel interface
public:
	bool setData(const QModelIndex &index, const QVariant &value, int role);
	QVariant headerData(int section, Qt::Orientation orientation, int role) const;

	// QAbstractItemModel interface
public:
	Qt::ItemFlags flags(const QModelIndex &index) const;

	// QAbstractItemModel interface
public:
	bool insertRows(int row, int count, const QModelIndex &parent);
	bool removeRows(int row, int count, const QModelIndex &parent);
};

#endif // SETTINGSTABLEADAPTER_H
