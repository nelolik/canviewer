#ifndef SYSTEC_H
#define SYSTEC_H
#include "inc/slcan.h"
#include <QThread>
#include "Systec/Include/Usbcan32.h"
//#include "Systec/Include/UsbCanLs.h"
#include "Systec/Include/UsbCanUp.h"
#include <QFile>
#include <QMutex>
#include "canhandler.h"

class Systec : public QObject, public CanHandler
{
    Q_OBJECT
private:
    uint baudRate;                  //Индек скорости сети
    tUcanHandle UcanHandle;         //Указатель на устройство
    BYTE deviceNbr;                 //Номер устройство, нужен, так как может быть больше одного
    enum Operation {GETLOG, SCANNET, WRITE, READMEM};
//	QMutex mutex;                   //Мьютекс флага продолжения потока
	bool isInited;

	void error(int err);            //Выводит ошибку USB-CAN конвертера

public:
	Systec(QObject *parent);
    ~Systec();
	void writeTo();                     //Функция передачи данных в контроллер
	bool getIsInited() const;

signals:
	void signalMessage(QString);       //выводит сообщени во все поля вывода


	// CanHandler interface
public:
	QByteArray readAnyMess(unsigned long *canID);                    //Функция сканирования сети ??? устаревшая
	bool isReady();
	bool init(uint baurRate);
	bool disconnect();
	bool setBaudRate(uint baudRateIdx);

};



#endif // SYSTEC_H
